using System.IO;
using System.Text.Json;
using Google.Cloud.Functions.Framework;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Microsoft.Extensions.Logging;

namespace DespatchNoteDropper
{
    public class Function : IHttpFunction
    {
        private readonly ILogger _logger;

        public Function(ILogger<Function> logger) =>
            _logger = logger;

        public async Task HandleAsync(HttpContext context)
        {
            _logger.LogInformation("Function received request");

            var request = context.Request;
            var despatchId = 0;

            using TextReader reader = new StreamReader(request.Body);
            var json = await reader.ReadToEndAsync();
            var body = JsonSerializer.Deserialize<JsonElement>(json);
            if (body.TryGetProperty("despatchId", out var property) && property.ValueKind == JsonValueKind.Number)
            {
                despatchId = property.GetInt32();
            }

            await PublishMessageWithCustomAttributesAsync("paverssso", "despatch-note", despatchId);
        }

        private async Task PublishMessageWithCustomAttributesAsync(string projectId, string topicId, int despatchId)
        {
            var topicName = TopicName.FromProjectTopic(projectId, topicId);
            var publisher = await PublisherClient.CreateAsync(topicName);

            var message = new PubsubMessage
            {
                Data = ByteString.CopyFromUtf8(despatchId.ToString()),
            };
            
            var result = await publisher.PublishAsync(message);
            _logger.LogInformation("Published message with Id: {Result}", result);
        }
    }
}
